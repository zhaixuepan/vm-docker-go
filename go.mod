module chainmaker.org/chainmaker/vm-docker-go/v2

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/localconf/v2 v2.2.1
	chainmaker.org/chainmaker/logger/v2 v2.2.1
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	chainmaker.org/chainmaker/utils/v2 v2.2.2
	github.com/docker/distribution v2.7.1+incompatible
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.2.0 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/mitchellh/mapstructure v1.4.2
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/atomic v1.7.0
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	google.golang.org/grpc v1.41.0
)
